<?php include('header.php'); ?>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-12">
                <!-- Blog Post -->
                <div class="card mb-4" id="card-wow">
                    <div class="card-body">
                        <h2 class="card-title"><i class="fad fa-user"></i> Login</h2>
                        <p class="text-center">
                            <?php
                            if (isset($_POST['login']))
                            {
                                //do insert
                                $password = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['password']));
                                $email = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['email']));

                                $bnet_pass = bin2hex(strrev(hex2bin(strtoupper(hash("sha256",strtoupper(hash("sha256", strtoupper($email)).":".strtoupper($password)))))));
								$bnet_upper_pass = strtoupper($bnet_pass);


                                //let's check login
                                $check_login = $mysqliA->query("SELECT * FROM `battlenet_accounts` WHERE `email` = '$email' AND `sha_pass_hash` = '$bnet_upper_pass';") or die (mysqli_error($mysqliA));
                                $num_acc = $check_login->num_rows;
                                if($num_acc < 1)
                                {
                                    echo '
                                        <div class="alert alert-warning" role="alert">
                                            <i class="fad fa-exclamation-circle"></i> There is no user with this email address!
                                        </div>
                                    ';
                                    header("refresh:3; url=$custdir/login.php");
                                }
                                else
                                {
                                    while($login = $check_login->fetch_assoc())
                                    {
                                        $_SESSION['id'] = $login['id'];
                                        echo '
                                            <div class="alert alert-success" role="alert">
                                                <i class="fad fa-check"></i> You are now logged in!
                                            </div>
                                        ';
                                        header("refresh:3; url=$custdir/ucp.php");
                                    }
                                }
                            }
                            else
                            {
                            ?>
                        <form name="login" method="post" action="" enctype="multipart/form-data">
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" placeholder="Your email address" required>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" placeholder="Password" required>
                            </div>
                            <button type="submit" name="login" class="btn btn-warning form-control"><i class="fad fa-sign-in"></i> Sing in</button>
                        </form>
						<br />
						<a href="<?php if($site_dir == '') { echo '/';} elseif ($site_dir != '') { echo '/'.$site_dir.'/';}  ?>recover.php">Lost your password ?</a>
                        <?php
                        }
                         ?>
                        <br/>
                        </p>
                    </div>
                    <div class="card-footer text-muted">
                    </div>
                </div>
            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

<?php include('footer.php'); ?>
