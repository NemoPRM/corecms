<?php
include ('header.php');
include ('sidebar.php');

?>
    <div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo $custdir; ?>/acp/">Dashboard</a>
            </li>
        </ol>
            <div class="row">
                <div class="col-xl-3 col-sm-6 mb-2">
                    <div class="card text-white bg-primary o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fad fa-users"></i>
                            </div>
                            <div class="mr-5">Total accounts: <span class="badge badge-danger"><?php echo $total_acc; ?></span></div>
                        </div>
                        <a class="card-footer text-white clearfix small z-1" href="<?php echo $custdir; ?>/acp/view-accounts.php">
                            <span class="float-left">Details</span>
                            <span class="float-right">
                                <i class="fas fa-angle-right"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <div class="col-xl-3 col-sm-6 mb-2">
                    <div class="card text-white bg-success o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fad fa-user-plus"></i>
                            </div>
                            <div class="mr-5">New accounts this month: <span class="badge badge-danger"><?php echo $new_acc; ?></span></div>
                        </div>
                        <a class="card-footer text-white clearfix small z-1" href="#">
                            <span class="float-left">Details</span>
                            <span class="float-right">
                                <i class="fas fa-angle-right"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <div class="col-xl-3 col-sm-6 mb-2">
                    <div class="card text-white bg-primary o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fab fa-paypal"></i>
                            </div>
                            <div class="mr-5">Total PayPal orders: <span class="badge badge-danger"><?php echo $paypal_orders; ?></span> </div>
                        </div>
                        <a class="card-footer text-white clearfix small z-1" href="<?php echo $custdir; ?>/acp/store-orders.php">
                            <span class="float-left">Details</span>
                            <span class="float-right">
                                <i class="fas fa-angle-right"></i>
                            </span>
                        </a>
                    </div>
                </div>
                <div class="col-xl-3 col-sm-6 mb-2">
                    <div class="card text-white bg-success o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fad fa-globe-americas"></i>
                            </div>
                            <div class="mr-5">Online accounts: <span class="badge badge-danger"><?php echo $online_acc; ?></span></div>
                        </div>
                        <a class="card-footer text-white clearfix small z-1" href="<?php echo $custdir; ?>/acp/online-accounts.php">
                            <span class="float-left">Details</span>
                            <span class="float-right">
                                <i class="fas fa-angle-right"></i>
                            </span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xl-3 col-sm-6 mb-2">
                    <div class="card text-white bg-primary o-hidden h-100">
                        <div class="card-body">
                            <div class="card-body-icon">
                                <i class="fad fa-newspaper"></i>
                            </div>
                            <div class="mr-5">Total news: <span class="badge badge-danger"><?php echo $num_news; ?></span></div>
                        </div>
                        <a class="card-footer text-white clearfix small z-1" href="<?php echo $custdir; ?>/acp/view-news.php">
                            <span class="float-left">Details</span>
                            <span class="float-right">
                                <i class="fas fa-angle-right"></i>
                            </span>
                        </a>
                    </div>
                </div>
            </div>

			<div class="card mb-3">
				<div class="card-header"><i class="text-success fad fa-server"></i> Server Status</div>
				<div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover table-dark" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Service Type</th>
                                <th>Service Name</th>
                                <th>Service Port</th>
                                <th>Service Status</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Service Type</th>
                                <th>Service Name</th>
                                <th>Service Port</th>
                                <th>Service Status</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            <?php
                            //logon server
                            $logon_port= "8081";
                            $connect = fsockopen("$site_realm", "$logon_port", $errno, $errstr, $timeout = 30);
                            if (!$connect)
                            {
                                echo "<tr>
                                    <td>Logon Server</td>
                                    <td>Auth Server</td>
                                    <td>" . $logon_port ,"</td>
                                    <td><span class='badge badge-danger'><i class='fal fa-arrow-circle-down'></i> Offline</span> </td>
                                    </tr>";
                            }
                            else
                            {
                                echo "<tr>
                                    <td>Logon Server</td>
                                    <td>Auth Server</td>
                                    <td>" . $logon_port ,"</td>
                                    <td><span class='badge badge-success'><i class='fal fa-arrow-circle-up'></i> Online</span> </td>
                                    </tr>";
                            }
                            fclose($connect);
                            //get realm status
                            $get_realm = $mysqliA->query("SELECT * FROM `realmlist`;");
                            while($realm = $get_realm->fetch_assoc())
                            {
                                $realm_name = $realm['name'];
                                $realm_ip = $realm['address'];
                                $realm_port = $realm['port'];
                                $connect = fsockopen("$realm_ip", "$realm_port", $errno, $errstr, $timeout = 30);
                                if (!$connect)
                                {
                                    echo "<tr>
                                    <td>Realm</td>
                                    <td>" . $realm_name . "</td>
                                    <td>" . $realm_port ,"</td>
                                    <td><span class='badge badge-danger'><i class='fal fa-arrow-circle-down'></i> Offline</span> </td>
                                    </tr>";
                                }
                                else
                                {
                                    echo "<tr>
                                    <td>Realm</td>
                                    <td>" . $realm_name . "</td>
                                    <td>" . $realm_port ,"</td>
                                    <td><span class='badge badge-success'><i class='fal fa-arrow-circle-up'></i> Online</span> </td>
                                    </tr>";
                                }
                                fclose($connect);
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
				</div>
			</div>
    </div>
    <!-- /.container-fluid -->
<?php
include ('footer.php');
?>