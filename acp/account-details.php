<?php
include (dirname(__FILE__) . '/header.php');
include (dirname(__FILE__) . '/sidebar.php');
?>
    <div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo $custdir; ?>/acp/">Dashboard</a>
            </li>

        </ol>
        <div class="card mb-3">
            <div class="card-header">
                <i class="fad fa-file-user"></i> Account details </div>
            <div class="card-body">
                <div class="table-responsive">
                    <?php
                    $accID = $_GET['id'];

                    $accounts_query = $mysqliA->query("SELECT * FROM `account` WHERE `id` = '$accID';") or die (mysqli_error($mysqliA));
                    $num_query = $accounts_query->num_rows;
                    if($num_query < 1)
                    {
                        echo 'Invalid account id';
                    }
                    else
                    {
                        while($res = $accounts_query->fetch_assoc())
                        {
                            $accountID = $res['id'];
                            $username = $res['username'];
                            $email = $res['email'];
                            $joindate = $res['joindate'];
                            $lastIP = $res['last_ip'];
                            $rank_query = $mysqliA->query("SELECT * FROM `account_access` WHERE `id` = '$accountID'") or die (mysqli_error($mysqliA));
                            $num_rank = $rank_query->num_rows;
                            if($num_rank < 1)
                            {
                                $class = 'success';
                                $rank = 'Standard User';
                            }
                            else
                            {
                                while($rank_res = $rank_query->fetch_assoc())
                                {
                                    $rank_result = $rank_res['gmlevel'];

                                    switch ($rank_result) {
                                        case "3":
                                            $class = 'danger';
                                            break;
                                        case "2":
                                            $class = 'warning';
                                            break;
                                        case "1":
                                            $class = 'primary';
                                            break;
                                    }

                                    switch ($rank_result) {
                                        case "3":
                                            $rank = 'Administrator';
                                            break;
                                        case "2":
                                            $rank = 'Game Master';
                                            break;
                                        case "1":
                                            $rank = 'Moderator';
                                            break;
                                    }
                                }
                            }


                            echo '
                                    <div>
                                       <p>'. $username .'</p>
                                       <p>'. $email .'</p>
                                       <p><span class="badge badge-'. $class .'">'. $rank .'</span></p>
                                       <p>'. $joindate .'</p>
                                       <p>'. $lastIP .'</p>
                                    </div>
                                    ';
                        }
                    }

                    ?>

                    <h2>Characters on this account!</h2>
                    <hr />
                    <table class="table table-bordered table-striped table-hover table-dark" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Character Name</th>
                            <th>Level</th>
                            <th>Race</th>
                            <th>Class</th>
                            <th>Money</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Character Name</th>
                            <th>Level</th>
                            <th>Race</th>
                            <th>Class</th>
                            <th>Money</th>
                            <th>Status</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        <?php
                        $char_query = $mysqliC->query("SELECT * FROM `characters` WHERE `account` = '$accID';") or die (mysqli_error($mysqliC));
                        $num_query = $char_query->num_rows;
                        if($num_query < 1)
                        {
                            echo '<tr><td colspan="5">There are no characters on this account</td></tr>';
                        }
                        else
                        {
                            while($res = $char_query->fetch_assoc())
                            {
                                $name = $res['name'];
                                $level = $res['level'];
                                $race = $res['race'];
                                $gender = $res['gender'];
                                $class = $res['class'];
                                $amount = $res['money'];
                                $status = $res['online'];
                                switch($status){
                                    case 0:
                                        $online = "<span class='badge badge-danger'>Offline</span>";
                                        break;
                                    case 1:
                                        $online = "<span class='badge badge-success'>Online</span>";
                                }

                                $gold = substr($amount, 0, -4);
                                $silver = substr($amount, -4, -2);
                                $copper = substr($amount, -2);

                                if ($gold == 0)
                                    $gold = 0;

                                if ($silver == 0)
                                    $silver = 0;

                                if ($copper == 0)
                                    $copper = 0;



                                echo '
                                    <tr>
                                       <td class="class-'. $class .'">'. $name .'</td>
                                       <td>'. $level .'</td>
                                       <td><img src="/images/races/'. $race .'_'. $gender .'.png" width="18" height="18"/></td>
                                       <td><img src="/images/classes/'. $class .'.png"  width="18" height="18"/></td>
                                       <td>'. $gold .'<i class="fad fa-coins gold"></i> '. $silver .'<i class="fad fa-coins silver"></i> '. $copper .'<i class="fad fa-coins cooper"></i></td>
                                       <td>' . $online . '</td>
                                    </tr>
                                    ';
                            }
                        }
                        ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
<?php
include (dirname(__FILE__) . '/footer.php');
?>