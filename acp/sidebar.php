<div id="wrapper">

    <!-- Sidebar -->
    <ul class="sidebar navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="<?php echo $custdir; ?>/acp/dashboard.php">
                <i class="fad fa-tachometer-alt"></i>
                <span>Dashboard</span>
            </a>
        </li>
		<li class="nav-item dropdown">
			<a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="fad fa-users-cog"></i>
				<span>Users</span>
			</a>
			<div class="dropdown-menu" aria-labelledby="pagesDropdown" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(5px, 56px, 0px);">
				<a class="dropdown-item" href="<?php echo $custdir; ?>/acp/view-accounts.php"><i class="fad fa-user-cog"></i> View accounts</a>
				<!-- <a class="dropdown-item" href="#"><i class="fad fa-user-plus"></i> Create new</a> -->
			</div>
		</li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fad fa-store"></i>
                <span>Store Settings</span>
            </a>
            <div class="dropdown-menu" aria-labelledby="pagesDropdown" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(5px, 56px, 0px);">
                <a class="dropdown-item" href="<?php echo $custdir; ?>/acp/store-items.php"><i class="fad fa-box-heart"></i> Store Items</a>
                <a class="dropdown-item" href="<?php echo $custdir; ?>/acp/store-orders.php"><i class="fad fa-receipt"></i> Store orders</a>
                <a class="dropdown-item" href="<?php echo $custdir; ?>/acp/store-categorys.php"><i class="fad fa-list-alt"></i> Store categorys</a>
            </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fad fa-cogs"></i>
                <span>CMS Settings</span>
            </a>
            <div class="dropdown-menu" aria-labelledby="pagesDropdown" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(5px, 56px, 0px);">
                <a class="dropdown-item" href="<?php echo $custdir; ?>/acp/site-settings.php"><i class="fad fa-globe-americas"></i> Site Settings</a>
                <a class="dropdown-item" href="<?php echo $custdir; ?>/acp/paypal-settings.php"><i class="fab fa-paypal"></i> PayPal Settings</a>
            </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fad fa-server"></i>
                <span>Server Settings</span>
            </a>
            <div class="dropdown-menu" aria-labelledby="pagesDropdown" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(5px, 56px, 0px);">
                <a class="dropdown-item" href="<?php echo $custdir; ?>/acp/realm-settings.php"><i class="fad fa-globe-americas"></i> Realm Settings</a>
            </div>
        </li>
    </ul>