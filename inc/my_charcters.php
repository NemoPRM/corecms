<?php
$sessid = $_SESSION['id'];

//let's do more stuff
$get_acc_id = $mysqliA->query("SELECT `id` FROM `account` WHERE `battlenet_account` = '$sessid';") or die (mysqli_errno($mysqliA));
while ($acc = $get_acc_id->fetch_assoc()) {
    $acc_id = $acc['id'];
}
//get characters
$get_chars_acc = $mysqliC->query("SELECT * FROM `characters` WHERE `account` = '$acc_id'") or die (mysqli_error($mysqliC));
$num_chars = $get_chars_acc->num_rows;
if ($num_chars < 1)
{
    echo '
        <div class="alert alert-info" role="alert">
          <i class="fad fa-exclamation-circle"></i> You don\'t have any characters on this account!
        </div>
    ';
}
else
{
    echo '
        <h5>Your characters</h5>
        <table class="table table-hover">
        <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Race</th>
            <th scope="col">Class</th>
            <th scope="col">Level</th>
        </tr>
        </thead>
        <tbody>
        ';
    while ($resc = $get_chars_acc->fetch_assoc()) {
        $char_name = $resc['name'];
        $char_level = $resc['level'];
        $char_race = $resc['race'];
        $char_gender = $resc['gender'];
        $char_class = $resc['class'];

        echo '
        <tr>
            <td class="class-' . $char_class . '">' . $char_name . '</td>
            <td><img src="/images/races/' . $char_race . '_' . $char_gender . '.png" width="16" height="16" alt="race"></td>
            <td><img src="/images/classes/' . $char_class . '.png" width="16" height="16" alt="class"></td>
            <td>' . $char_level . '</td>
        </tr>
        ';
    }
    echo '
        </tbody>
    </table>
    ';
}