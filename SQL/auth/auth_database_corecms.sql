/***************************/
/* IMPORT TO AUTH DATABASE */
/***************************/


SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;




-- ----------------------------
-- Table structure for payments
-- ----------------------------
DROP TABLE IF EXISTS `payments`;
CREATE TABLE `payments`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `payment_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `payer_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `payer_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `amount` float(10, 2) NOT NULL,
  `currency` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `payment_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `coins_claimed` int(2) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;


-- ----------------------------
-- Table structure for site_settings
-- ----------------------------
DROP TABLE IF EXISTS `site_settings`;
CREATE TABLE `site_settings`  (
  `site_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `site_description` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `site_contact` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `site_theme` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `site_realm` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `site_discord` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `site_en_don` INT(5) NOT NULL DEFAULT 1,
  `site_subdirectory` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL NULL DEFAULT '',
  `paypal_client_id` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `paypal_client_secret` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `paypal_return_url` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `paypal_cancel_url` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `paypal_currency` varchar(3) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `paypal_status` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`site_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of site_settings
-- ----------------------------
INSERT INTO `site_settings` VALUES ('ShadowCore', 'ShadowCore', 'support@ShadowCore.com', 'shadowlands', 'localhost', 'https://discord.gg/57D59ed', '1', '', 'Your PayPal ClientID', 'Your PayPal Client Secred Key', 'http://www.myserver.com/success.php', 'http://www.myserver.com/cancel.php', 'EUR', 'false');


-- ----------------------------
-- Table structure for store_items
-- ----------------------------
DROP TABLE IF EXISTS `store_items`;
CREATE TABLE `store_items`  (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `item_id` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `item_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `price` decimal(10, 0) NULL DEFAULT NULL,
  `category` INT(5) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of store_items
-- ----------------------------
INSERT INTO `store_items` (`id`, `item_id`, `item_name`, `price`, `category`) VALUES
('1','139739','Chestguard of the Uncrowned','300','1'),
('2','139740','Footpads of the Uncrowned','300','1'),
('3','139741','Gloves of the Uncrowned','300','1'),
('4','139742','Mask of the Uncrowned','300','1'),
('5','139743','Leggings of the Uncrowned','300','1'),
('6','139744','Shoulderblades of the Uncrowned','300','1'),
('7','139745','Belt of the Uncrowned','300','1'),
('8','139746','Wristbands of the Uncrowned','300','1'),
('9','172199',"Faralos, Empire's Dream",'300','1'),
('10','174105',"Mish'un, Blade of Tyrants",'300','1'),
('11','165566',"Lord admiral's signet",'300','1'),
('12','165567','Seal of the zandalari empire','300','1'),
('13','174500','Vita-charged titanshard','300','1'),
('14','174528','Void-twisted titanshard','300','1'),
('15','169223',"Ashjra'kamas, Shroud of Resolve",'300','1'),
('16','98543','Wraps of the Blood-Soaked Brawler','300','1'),
('17','38311','Tabard of the Void','300','1'),
('18','158075','Heart Of Azeroth','300','1');

-- ----------------------------
-- Table structure for web_user
-- ----------------------------

CREATE TABLE IF NOT EXISTS `web_user` (
  `id` int(5) NOT NULL,
  `username` VARCHAR(45) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of web_user
-- ----------------------------

INSERT INTO `web_user` (`id`, `username`) VALUES ('1','GM');

-- ----------------------------
-- Table structure for web_news
-- ----------------------------

CREATE TABLE IF NOT EXISTS `web_news` (
  `id` INT(5) NOT NULL AUTO_INCREMENT,
  `header` VARCHAR(45) NULL,
  `news` TEXT NULL,
  `author` VARCHAR(45) NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
  ) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

  -- ----------------------------
-- Records of web_news
-- ----------------------------

INSERT INTO `web_news` (`header`, `news`, `author`) VALUES ('Example', 'This is an example News, you can delete it in the ACP', 'GM');

-- ----------------------------
-- Table structure for store_items_categorys
-- ----------------------------

CREATE TABLE IF NOT EXISTS `store_items_categorys` (
  `id` INT(5) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));

-- ----------------------------
-- Records of store_items_categorys
-- ----------------------------

INSERT INTO `store_items_categorys` (`id`, `name`) VALUES
('1', 'Equipables'),
('2', 'Mounts'),
('3', 'Pets');



SET FOREIGN_KEY_CHECKS = 1;

